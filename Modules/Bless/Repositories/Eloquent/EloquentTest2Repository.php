<?php namespace Modules\Bless\Repositories\Eloquent;

use Modules\Bless\Repositories\Test2Repository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

class EloquentTest2Repository extends EloquentBaseRepository implements Test2Repository
{
}
