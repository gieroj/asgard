<?php namespace Modules\Bless\Repositories\Eloquent;

use Modules\Bless\Repositories\TestRepository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

class EloquentTestRepository extends EloquentBaseRepository implements TestRepository
{
}
