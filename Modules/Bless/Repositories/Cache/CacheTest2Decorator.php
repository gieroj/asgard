<?php namespace Modules\Bless\Repositories\Cache;

use Modules\Bless\Repositories\Test2Repository;
use Modules\Core\Repositories\Cache\BaseCacheDecorator;

class CacheTest2Decorator extends BaseCacheDecorator implements Test2Repository
{
    public function __construct(Test2Repository $test2)
    {
        parent::__construct();
        $this->entityName = 'bless.test2s';
        $this->repository = $test2;
    }
}
