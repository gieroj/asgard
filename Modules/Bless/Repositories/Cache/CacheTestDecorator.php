<?php namespace Modules\Bless\Repositories\Cache;

use Modules\Bless\Repositories\TestRepository;
use Modules\Core\Repositories\Cache\BaseCacheDecorator;

class CacheTestDecorator extends BaseCacheDecorator implements TestRepository
{
    public function __construct(TestRepository $test)
    {
        parent::__construct();
        $this->entityName = 'bless.tests';
        $this->repository = $test;
    }
}
