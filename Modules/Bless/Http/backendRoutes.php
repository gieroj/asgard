<?php

use Illuminate\Routing\Router;
/** @var Router $router */

$router->group(['prefix' =>'/bless'], function (Router $router) {
        $router->bind('tests', function ($id) {
            return app('Modules\Bless\Repositories\TestRepository')->find($id);
        });
        $router->resource('tests', 'TestController', ['except' => ['show'], 'names' => [
            'index' => 'admin.bless.test.index',
            'create' => 'admin.bless.test.create',
            'store' => 'admin.bless.test.store',
            'edit' => 'admin.bless.test.edit',
            'update' => 'admin.bless.test.update',
            'destroy' => 'admin.bless.test.destroy',
        ]]);
        $router->bind('test2s', function ($id) {
            return app('Modules\Bless\Repositories\Test2Repository')->find($id);
        });
        $router->resource('test2s', 'Test2Controller', ['except' => ['show'], 'names' => [
            'index' => 'admin.bless.test2.index',
            'create' => 'admin.bless.test2.create',
            'store' => 'admin.bless.test2.store',
            'edit' => 'admin.bless.test2.edit',
            'update' => 'admin.bless.test2.update',
            'destroy' => 'admin.bless.test2.destroy',
        ]]);
// append


});
