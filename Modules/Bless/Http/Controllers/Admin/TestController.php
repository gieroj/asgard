<?php namespace Modules\Bless\Http\Controllers\Admin;

use Laracasts\Flash\Flash;
use Illuminate\Http\Request;
use Modules\Bless\Entities\Test;
use Modules\Bless\Repositories\TestRepository;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;

class TestController extends AdminBaseController
{
    /**
     * @var TestRepository
     */
    private $test;

    public function __construct(TestRepository $test)
    {
        parent::__construct();

        $this->test = $test;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //$tests = $this->test->all();

        return view('bless::admin.tests.index', compact(''));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('bless::admin.tests.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $this->test->create($request->all());

        flash()->success(trans('core::core.messages.resource created', ['name' => trans('bless::tests.title.tests')]));

        return redirect()->route('admin.bless.test.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Test $test
     * @return Response
     */
    public function edit(Test $test)
    {
        return view('bless::admin.tests.edit', compact('test'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Test $test
     * @param  Request $request
     * @return Response
     */
    public function update(Test $test, Request $request)
    {
        $this->test->update($test, $request->all());

        flash()->success(trans('core::core.messages.resource updated', ['name' => trans('bless::tests.title.tests')]));

        return redirect()->route('admin.bless.test.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Test $test
     * @return Response
     */
    public function destroy(Test $test)
    {
        $this->test->destroy($test);

        flash()->success(trans('core::core.messages.resource deleted', ['name' => trans('bless::tests.title.tests')]));

        return redirect()->route('admin.bless.test.index');
    }
}
