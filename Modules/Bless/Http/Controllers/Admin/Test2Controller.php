<?php namespace Modules\Bless\Http\Controllers\Admin;

use Laracasts\Flash\Flash;
use Illuminate\Http\Request;
use Modules\Bless\Entities\Test2;
use Modules\Bless\Repositories\Test2Repository;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;

class Test2Controller extends AdminBaseController
{
    /**
     * @var Test2Repository
     */
    private $test2;

    public function __construct(Test2Repository $test2)
    {
        parent::__construct();

        $this->test2 = $test2;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //$test2s = $this->test2->all();

        return view('bless::admin.test2s.index', compact(''));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('bless::admin.test2s.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $this->test2->create($request->all());

        flash()->success(trans('core::core.messages.resource created', ['name' => trans('bless::test2s.title.test2s')]));

        return redirect()->route('admin.bless.test2.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Test2 $test2
     * @return Response
     */
    public function edit(Test2 $test2)
    {
        return view('bless::admin.test2s.edit', compact('test2'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Test2 $test2
     * @param  Request $request
     * @return Response
     */
    public function update(Test2 $test2, Request $request)
    {
        $this->test2->update($test2, $request->all());

        flash()->success(trans('core::core.messages.resource updated', ['name' => trans('bless::test2s.title.test2s')]));

        return redirect()->route('admin.bless.test2.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Test2 $test2
     * @return Response
     */
    public function destroy(Test2 $test2)
    {
        $this->test2->destroy($test2);

        flash()->success(trans('core::core.messages.resource deleted', ['name' => trans('bless::test2s.title.test2s')]));

        return redirect()->route('admin.bless.test2.index');
    }
}
