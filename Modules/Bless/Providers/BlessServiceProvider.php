<?php namespace Modules\Bless\Providers;

use Illuminate\Support\ServiceProvider;

class BlessServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->registerBindings();
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return array();
    }

    private function registerBindings()
    {
        $this->app->bind(
            'Modules\Bless\Repositories\TestRepository',
            function () {
                $repository = new \Modules\Bless\Repositories\Eloquent\EloquentTestRepository(new \Modules\Bless\Entities\Test());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new \Modules\Bless\Repositories\Cache\CacheTestDecorator($repository);
            }
        );
        $this->app->bind(
            'Modules\Bless\Repositories\Test2Repository',
            function () {
                $repository = new \Modules\Bless\Repositories\Eloquent\EloquentTest2Repository(new \Modules\Bless\Entities\Test2());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new \Modules\Bless\Repositories\Cache\CacheTest2Decorator($repository);
            }
        );
// add bindings


    }
}
