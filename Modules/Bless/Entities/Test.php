<?php namespace Modules\Bless\Entities;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class Test extends Model
{
    use Translatable;

    protected $table = 'bless__tests';
    public $translatedAttributes = [];
    protected $fillable = [];
}
