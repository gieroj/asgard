<?php namespace Modules\Bless\Entities;

use Illuminate\Database\Eloquent\Model;

class Test2Translation extends Model
{
    public $timestamps = false;
    protected $fillable = [];
    protected $table = 'bless__test2_translations';
}
