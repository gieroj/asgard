<?php namespace Modules\Bless\Entities;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class Test2 extends Model
{
    use Translatable;

    protected $table = 'bless__test2s';
    public $translatedAttributes = [];
    protected $fillable = [];
}
