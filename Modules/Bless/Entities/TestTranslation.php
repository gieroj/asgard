<?php namespace Modules\Bless\Entities;

use Illuminate\Database\Eloquent\Model;

class TestTranslation extends Model
{
    public $timestamps = false;
    protected $fillable = [];
    protected $table = 'bless__test_translations';
}
