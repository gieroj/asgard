<?php

return [
    'title' => [
        'tests' => 'Test',
        'create test' => 'Create a test',
        'edit test' => 'Edit a test',
    ],
    'button' => [
        'create test' => 'Create a test',
    ],
    'table' => [
    ],
    'form' => [
    ],
    'messages' => [
    ],
    'validation' => [
    ],
];
