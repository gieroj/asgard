<?php

return [
    'title' => [
        'test2s' => 'Test2',
        'create test2' => 'Create a test2',
        'edit test2' => 'Edit a test2',
    ],
    'button' => [
        'create test2' => 'Create a test2',
    ],
    'table' => [
    ],
    'form' => [
    ],
    'messages' => [
    ],
    'validation' => [
    ],
];
